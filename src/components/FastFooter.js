import React, {Component} from 'react';

class FastFooter extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="container">
                    <div className="row fastFooter">
                        <div className="col-md-7 col-12">
                            <p>Made With Love By Figmaland All Right Reserved </p>
                        </div>
                        <div className="col-md-5 col-12 text-end">
                            <img className="lastFooterImgs" src="images/facebook.svg" alt=""/>
                            <img className="lastFooterImgMobile" src="images/facebookMobile.svg" alt=""/>
                            <img className="lastFooterImgs" src="images/instagram.svg" alt=""/>
                            <img className="lastFooterImgMobile" src="images/instagramMobile.svg" alt=""/>
                            <img className="lastFooterImgs" src="images/twitter.svg" alt=""/>
                            <img className="lastFooterImgMobile" src="images/twitterMobile.svg" alt=""/>
                            <img className="lastFooterImgMobile" src="images/youtubeMobile.svg" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FastFooter;