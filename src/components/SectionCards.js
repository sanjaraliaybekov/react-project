import React, {Component} from 'react';
import {Col} from "reactstrap";
import AOS from 'aos'
class SectionCards extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        const {sectionCards} = this.props;
        return (
            <div className="row text-center text-card justify-content-between">
                <div className="col-md-12">
                    <div>
                        <h1>Industry</h1>
                        <h3>Problems trying to resolve the conflict between <br/>
                            the two major realms of Classical physics: Newtonian mechanics </h3>
                    </div>
                </div>
                {sectionCards.map((item, index) => (
                    <Col key={index} md={4} data-aos="fade-up"
                         data-aos-anchor-placement="top-center" data-aos-duration="3000">
                        <div className="sectionCards" >
                            <div className="card">
                                <div className="card-header">
                                    <img src={item.img} alt=""/>
                                </div>
                                <div className="card-body">
                                    <h2>{item.title}</h2>
                                </div>
                                <div className="card-footer">
                                    <h5><img src="images/fluitStar.svg"/><img className="mx-1" src="images/fluitStar.svg"/>
                                        <img src="images/fluitStar.svg"/><img className="mx-1"  src="images/fluitStar.svg"/>
                                        <img src="images/notFluitStar.svg"/></h5>
                                </div>
                            </div>
                        </div>
                    </Col>
                ))}
            </div>
        );
    }
}

export default SectionCards;