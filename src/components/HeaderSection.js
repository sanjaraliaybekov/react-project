import React, {Component} from 'react';
import {Col, Container, Row} from "reactstrap";
import AOS from 'aos'
class HeaderSection extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        const {headerCard} = this.props;
        return (
            <Container>
                <Row className="headerSection">
                    <Col md={12} className="text-center text-white header-title-button" data-aos="fade-down"
                         data-aos-duration="3000">
                        <h1>We Ensure A Best <br/> Insurance Service</h1>
                        <h3>We know how large objects will act, but things on a <br/>
                            small scale just do not act that way.</h3>
                        <div className="buttons">
                            <button className="btn btn-outline-light" data-aos="fade-right"
                                    // data-aos-delay="1000"
                                    data-aos-anchor="#example-anchor"
                                    data-aos-offset="500"
                                    data-aos-duration="3000">Get Quote Now</button>
                            <button className="btn btn-outline-light" data-aos="fade-left"
                                // data-aos-delay="1000"
                                    data-aos-anchor="#example-anchor"
                                    data-aos-offset="500"
                                    data-aos-duration="3000">Learn More</button>
                        </div>
                    </Col>
                    <Col md={12} className="position-relative">
                        <Row className="header-cards-row">
                            {headerCard.map((item, index) => (
                                <Col key={index} md={4} data-aos="flip-left"
                                     data-aos-easing="ease-out-cubic"
                                     data-aos-duration="2000">
                                    <div className="headerCards">
                                        <img src={item.img} alt=""/>
                                        <h2>{item.title}</h2>
                                        <h5>{item.paragraph}</h5>
                                    </div>
                                </Col>
                            ))}
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default HeaderSection;