import React, {Component} from 'react';
import {Col} from "reactstrap";
import AOS from 'aos';
class SectionBrands extends Component {
    componentDidMount() {
        AOS.init();
    }
    render() {
        return (
            <div className="row">
                <Col md={12}>
                    <div className="brand row">
                        <div className="col-md-2 col-12">
                            <img src="images/holi.svg" alt=""/>
                        </div>
                        <div className="col-md-2 col-12">
                            <img src="images/lyft.svg" alt=""/>

                        </div>
                        <div className="col-md-2 col-12" >
                            <img src="images/barg.svg" alt=""/>

                        </div>
                        <div className="col-md-2 col-12">
                            <img src="images/stripe.svg" alt=""/>

                        </div>
                        <div className="col-md-2 col-12">
                            <img src="images/aws.svg" alt=""/>
                        </div>
                        <div className="col-md-2 col-12">
                            <img src="images/panda.svg" alt=""/>
                        </div>
                    </div>
                </Col>
                <Col md={12} className="input-mobile-col">
                    <div className="text-input justify-content-between d-flex align-items-center">
                        <div className="text">
                            <h3>Subscribe For Latest <br/>
                                Newsletter</h3>
                        </div>
                        <div className="input d-flex">
                            <input type="email" value="Your Email" className="form-control"/>
                            <button className="btn btn-primary">Subscribe</button>
                        </div>
                    </div>
                </Col>
            </div>
        );
    }
}

export default SectionBrands;