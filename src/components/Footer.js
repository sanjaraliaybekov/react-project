import React, {Component} from 'react';
import AOS from 'aos'
class Footer extends Component {
   componentDidMount() {
       AOS.init();
   }

    render() {
        return (
            <div className="container-fluid bg-white">
                <div className="container footer">
                    <div className="row pt-5">
                        <div className="col-md-2 col-12" data-aos="flip-right" data-aos-duration="2000">
                            <p>Company Info</p>
                            <ul type="none" className="p-0">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Carrier</a></li>
                                <li><a href="#">We are hearing</a></li>
                                <li><a href="#">Blog</a></li>
                            </ul>
                        </div>
                        <div className="col-md-2 col-12" data-aos="flip-right" data-aos-duration="2000">
                            <p>Legal</p>
                            <ul type="none" className="p-0">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Carrier</a></li>
                                <li><a href="#">We are hearing</a></li>
                                <li><a href="#">Blog</a></li>
                            </ul>
                        </div>
                        <div className="col-md-2 col-12" data-aos="flip-up"
                        data-aos-duration="2000">
                            <p>Features</p>
                            <ul type="none" className="p-0">
                                <li ><a href="#">Business Marketing</a></li>
                                <li><a href="#">User Analytic</a></li>
                                <li><a href="#">Live Chat</a></li>
                                <li><a href="#">Unlimited Support</a></li>
                            </ul>
                        </div>
                        <div className="col-md-2 col-12" data-aos-duration="2000" data-aos="flip-down">
                            <p>Resources</p>
                            <ul type="none" className="p-0">
                                <li><a href="#">IOS & Android</a></li>
                                <li><a href="#">Watch a Demo</a></li>
                                <li><a href="#">Customers</a></li>
                                <li><a href="#">API</a></li>
                            </ul>
                        </div>
                        <div className="col-md-4 col-12" data-aos="zoom-in" data-aos-duration="2000">
                            <p>Get In Touch</p>
                            <ul type="none" className="p-0">
                                <li><img src="images/bx_bx-phone.svg" alt=""/><a href="#">(480) 555-0103</a></li>
                                <li><img src="images/bx_bx-map.svg" alt=""/><a href="#">4517 Washington Ave.</a></li>
                                <li><img src="images/carbon_send-alt.svg" alt=""/><a href="#">debra.holt@example.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;