import React, {Component} from 'react';
import {Col} from "reactstrap";
import AOS from 'aos'

class SectionCards extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        const {scaleCards} = this.props;
        return (
            <div className="row text-center scale-cards justify-content-between">
                <div className="col-md-12" data-aos="fade-right">
                    <div>
                        <h1>Pricing</h1>
                        <h3>Problems trying to resolve the conflict between <br/>
                            the two major realms of Classical physics: Newtonian mechanics </h3>
                    </div>
                </div>
                {scaleCards.map((item, index) => (
                    <Col key={index} md={4} data-aos="fade-down-right"
                    data-aos-duration="1500">

                        <div className="scaleCards">
                            <div className="card">
                                <div className="card-header">
                                    <h1>{item.title1}</h1>
                                    <h2>{item.title2}</h2>
                                    <div className="d-flex justify-content-center">
                                        <div><h3>{item.price1}</h3>
                                        </div>
                                        <div className="d-flex flex-column">
                                            <div className="d-flex align-items-start">
                                                <h4>
                                                    {item.price2}
                                                </h4>
                                            </div>
                                            <div>
                                                <h5>
                                                    {item.month}
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <ul type="none">
                                        <li className="d-flex align-items-center"><img src="images/ptichka-green.svg"
                                                                                       alt=""/><h6>{item.title3}</h6>
                                        </li>
                                        <li className="d-flex align-items-center"><img src="images/ptichka-green.svg"
                                                                                       alt=""/><h6>{item.title3}</h6>
                                        </li>
                                        <li className="d-flex align-items-center"><img src="images/ptichka-green.svg"
                                                                                       alt=""/><h6>{item.title3}</h6>
                                        </li>
                                        <li className="d-flex align-items-center"><img src="images/ptichka-gray.svg"
                                                                                       alt=""/><h6>{item.title4}</h6>
                                        </li>
                                        <li className="d-flex"><img src="images/ptichka-gray.svg"
                                                                                       alt=""/><h6>{item.title5}</h6>
                                        </li>
                                    </ul>
                                </div>
                                <div className="card-footer">
                                    <button className="btn btn-primary">{item.buttonText}</button>
                                </div>
                            </div>
                        </div>
                    </Col>
                ))}
            </div>
        );
    }
}

export default SectionCards;