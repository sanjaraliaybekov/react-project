import React, {Component} from 'react';
import HeaderNav from "./HeaderNav";
import HeaderSection from "./HeaderSection";
import Main from "./Main";

class Header extends Component {
    render() {
        const {headerCard} = this.props;
        return (
            <div className="header">
                <HeaderNav/>
                <HeaderSection headerCard={headerCard}/>
            </div>
        );
    }
}

export default Header;