import React, {Component} from 'react';
import {Col} from "reactstrap";
import AOS from "aos"

class SectionBox extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        const {sectionBox} = this.props;
        return (
            <div className="row sectionBox">
                {sectionBox.map((item, index) => (
                    <Col key={index} md={4} data-aos="fade-right">
                        <div>
                            <img src={item.img} alt=""/>
                            <h2>{item.title}</h2>
                            <h5>{item.paragraph}</h5>
                        </div>
                    </Col>
                ))}
            </div>
        );
    }
}

export default SectionBox;