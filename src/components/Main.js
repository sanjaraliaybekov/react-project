import React, {Component} from 'react';
import AOS from 'aos'
class Main extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        return (
            <div className="container-fluid main">
                <div className="container position-relative">
                    <div className="row main-content justify-content-center align-items-center">
                        <div className="col-md-7 col-12 h-100">
                            <div>
                                <h3>
                                    We Have Branches All <br/>
                                    Over The World
                                </h3>
                                <p className="mt-3">The gradual accumulation of information about atomic and <br/>
                                    small-scale behaviour during the first quarter of the 20th <br/>
                                    century, which gave some indications about how small things <br/>
                                    do behave, produced an increasing confusion which was <br/>
                                    Heisenberg, and Born.</p>
                            </div>
                        </div>
                        <div className="col-md-5 earth col-12 h-100 d-flex justify-content-end">
                            <div className="w-100"><img className="w-100" src="images/earth.svg" alt=""/></div>
                        </div>
                    </div>
                    <div className="d-flex justify-content-center" data-aos="flip-left"
                         data-aos-easing="ease-out-cubic"
                         data-aos-duration="2000">
                        <div className="main-input d-flex">
                            <div className="h-100 w-100">
                                <img className="onlyDesktop" src="images/media.svg" alt=""/>
                                <img className="w-100 onlyMobile" src="images/family.svg" alt=""/>
                            </div>
                            <div className="input-area w-100">
                                <h3 className="onlyDesktop">Book Appointment</h3>
                                <h3 className="onlyMobile">Get A Free <br/>
                                    Quote Here</h3>
                                <form>
                                    <label htmlFor="name">Name *</label>
                                    <input type="text" className="form-control" placeholder="Full Name *" id="name"/>

                                    <label htmlFor="email">Email address *</label>
                                    <input type="email" className="form-control" placeholder="example@gmail.com *"
                                           id="email"/>

                                    <label htmlFor="department">Departement *</label>
                                    <select id="department" className="form-control">
                                        <option>Departement *</option>
                                        <option>Departement *</option>
                                        <option>Departement *</option>
                                    </select>
                                    <label htmlFor="data">Time *</label>
                                    <input type="date" id="data" className="form-control" placeholder="4:00 Available"/>
                                </form>
                                <button type="button" className="btn btn-primary w-100">
                                    Book Appointment
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Main;