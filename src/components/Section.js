import React, {Component} from 'react';
import SectionBox from "./SectionBox";
import SectionCards from "./SectionCards";
import SectionBrands from "./SectionBrands";
import ScaleCards from "./ScaleCards";
import Main from "./Main";

class Section extends Component {
    render() {
        const {sectionBox, sectionCards,scaleCards} = this.props;
        return (
            <div className="container section">
                <SectionBox sectionBox={sectionBox}/>
                <SectionCards sectionCards={sectionCards}/>
                <SectionBrands/>
                <ScaleCards scaleCards={scaleCards}/>

            </div>
        );
    }
}

export default Section;