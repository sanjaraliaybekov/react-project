import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import AOS from 'aos'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    NavbarText, Container, Row, Col
} from 'reactstrap';

class HeaderNav extends Component {
    componentDidMount() {
        AOS.init();
    }

    state = {
        isOpen: false
    };
    toggle = () => this.setState({isOpen: !this.state.isOpen});

    render() {
        return (
            <div>
                <Container>
                    <Row  className="nav-row position-relative">
                        <Col md={12} data-aos="zoom-in" data-aos-duration="2    000">
                            <Navbar expand="md">
                                <NavbarBrand className="nav-brand" href="/">BrandName</NavbarBrand>
                                <NavbarToggler className="nav-tog" onClick={this.toggle}/>

                                    <Collapse isOpen={this.state.isOpen} navbar>
                                    <Nav navbar>
                                        <NavItem>
                                            <NavLink href="#">Home</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#">Product</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#">Pricing</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#">Contact</NavLink>
                                        </NavItem>
                                    </Nav>
                                </Collapse>

                                <Nav>
                                    <NavItem>
                                        <NavLink href="#" className="onlyDesktop">Login</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <button className="btn btn-primary onlyDesktop">Become a member <img
                                            src="images/arrowR.svg" alt=""/></button>
                                    </NavItem>
                                </Nav>
                            </Navbar>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default HeaderNav;