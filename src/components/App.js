import React, {Component} from 'react';
import Header from "./Header";
import Section from "./Section";
import Main from "./Main";
import AfterInput from "./AfterInput";
import Footer from "./Footer";
import FastFooter from "./FastFooter";


class App extends Component {
    state = {
        headerCard: [
            {
                img: "images/doubleMan.svg",
                title: "Peace of Mind",
                paragraph: "the quick fox jumps over the \n" +
                    "lazy dog"
            },
            {
                img: "images/doubleMan.svg",
                title: "Set For Life",
                paragraph: "the quick fox jumps over the \n" +
                    "lazy dog"
            },
            {
                img: "images/doubleMan.svg",
                title: "100% Satisfaction",
                paragraph: "the quick fox jumps over the \n" +
                    "lazy dog"
            }
        ],
        sectionBox: [
            {
                img: "images/box1.svg",
                title: "Peace of Mind",
                paragraph: "So it really behaves like neither. \n" +
                    "Now we have given up."
            },
            {
                img: "images/box2.svg",
                title: "Set For Life",
                paragraph: "They were used to create the \n" +
                    "machines that launched "
            },
            {
                img: "images/box3.svg",
                title: "100% Satisfaction",
                paragraph: "So it really behaves like neither. \n" +
                    "Now we have given up."
            }
        ],
        sectionCards: [
            {
                img: "images/card1.svg",
                title: "Slate helps you see how \n" +
                    "many more days you need \n" +
                    "to work to reach your financial \n" +
                    "goal for the month and year.",
                paragraph: "So it really behaves like neither. \n" +
                    "Now we have given up."
            },
            {
                img: "images/card2.svg",
                title: "Slate helps you see how \n" +
                    "many more days you need \n" +
                    "to work to reach your financial \n" +
                    "goal for the month and year.",
                paragraph: "They were used to create the \n" +
                    "machines that launched "
            },
            {
                img: "images/card3.svg",
                title: "Slate helps you see how \n" +
                    "many more days you need \n" +
                    "to work to reach your financial \n" +
                    "goal for the month and year.",
                paragraph: "So it really behaves like neither. \n" +
                    "Now we have given up."
            }
        ],
        scaleCards: [
            {
                title1: "FREE",
                title2: "Organize across all \n" +
                    "apps by hand",
                price1: "0",
                price2: "$",
                month: "Per Month",
                img1: "images/ptichka-gray.svg",
                img2: "images/ptichka-green.svg",
                title3: "Unlimited product updates\n",
                title4: "1GB  Cloud storage\n",
                title5: "Email and community \n" +
                    "support\n",
                buttonText: "Try for free"
            },
            {
                title1: "STANDART",
                title2: "Organize across all \n" +
                    "apps by hand",
                price1: "9.99",
                price2: "$",
                month: "Per Month",
                img1: "images/ptichka-green.svg",
                img2: "images/ptichka-gray.svg",
                title3: "Unlimited product updates\n",
                title4: "1GB  Cloud storage\n",
                title5: "Email and community \n" +
                    "support\n",
                buttonText: "Try for free"
            },
            {
                title1: "PREMIUM",
                title2: "Organize across all \n"+
                    "apps by hand",
                price1: "19.99",
                price2: "$",
                month: "Per Month",
                img1: "images/ptichka-green.svg",
                img2: "images/ptichka-gray.svg",
                title3: "Unlimited product updates\n",
                title4: "1GB  Cloud storage\n",
                title5: "Email and community \n" +
                    "support\n",
                buttonText: "Try for free"
            }
        ]
    };

    render() {
        const {headerCard, sectionBox, sectionCards, scaleCards} = this.state;
        return (
            <div>
                <Header headerCard={headerCard}/>
                <Section sectionBox={sectionBox} sectionCards={sectionCards} scaleCards={scaleCards}/>
                <Main/>
                <AfterInput/>
                <Footer/>
                <FastFooter/>
            </div>
        );
    }
}

export default App;