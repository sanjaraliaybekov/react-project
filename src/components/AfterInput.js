import React, {Component} from 'react';
import {Col} from "reactstrap";

class AfterInput extends Component {
    render() {
        return (
            <div className="container">
                <div className="row after-input mb-5">
                    <Col md={12}>
                        <div className="text-input justify-content-between d-flex align-items-center">
                            <div className="text">
                                <h3>Consulting Agency <br className="onlyMobile"/> For Your Business</h3>
                                <p>the quick fox jumps over <br className="onlyMobile"/> the lazy dog</p>
                            </div>
                            <div className="input">
                                <button className="btn btn-primary">Contact Us</button>
                            </div>
                        </div>
                    </Col>
                </div>

            </div>
        );
    }
}

export default AfterInput;